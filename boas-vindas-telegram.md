Olá! Somos um grupo de pessoas entusiastas da filosofia do Software livre.

Este grupo reflete as discussões da sala na [Matrix](https://matrix.to/#/#comunidadesoftwarelivre:matrix.org), que é um software com clientes, apps e servidores livres.

Leia nosso [código de conduta](https://gitlab.com/comunidade-software-livre/gestao/-/blob/main/codigo-de-conduta.md). É assumido que você concorda com ele ao fazer as suas contribuições.

Na descrição do grupo e nas mensagens fixadas você encontra mais detalhes de como nos organizamos.

E para termos certeza de que você não é um robô, por gentileza, nos diga qual o seu software livre favorito assim que entrar no grupo. =)
